
angular.module('slush', [
  'ngRoute',
  'slush.todo'
])
.config(function ($routeProvider) {
  'use strict';
  $routeProvider
    .when('/todo', {
      controller: 'TodoCtrl',
      templateUrl: '/slush/todo/todo.html'
    })
    .otherwise({
      redirectTo: '/todo'
    });
});
